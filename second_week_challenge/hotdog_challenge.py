from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D
from tensorflow.keras.layers import Activation, Dropout, Flatten, Dense
from tensorflow.keras import backend as KBackend
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.utils import plot_model
import matplotlib.pyplot as plt


# dimensions of our images.
img_width, img_height = 150, 150

train_data_dir = 'data/train'
validation_data_dir = 'data/validation'
nb_train_samples = 240
nb_validation_samples = 130

# Define epoch for training
epochs =
# Lower the batch size if you're training on GPU and get out of memory errors
batch_size =

# Checks what the default image data format is for Tensorflow backend
if KBackend.image_data_format() == 'channels_first':
    input_shape = (3, img_width, img_height)
else:
    input_shape = (img_width, img_height, 3)


# A simple 3 layer convolution network with ReLU activation

#Layer 1 - A 2D Convolution Layer with 32 filters, a kernal size of 3,3 and a 1,1 stride
#          and an input shape defined by the shape of our image.
#          It has a ReLu activation function and we include a 2D Max Pooling layer with a pool size of 2,2




#Layer 2 - A 2D Convolution Layer with 32 filters, a kernal size of 3,3 and a 1,1 stride
#          It has a ReLu activation function and we include a 2D Max Pooling layer with a pool size of 2,2




#Layer 3 - A 2D Convolution Layer with 64 filters and a kernal size of 3,3.
#          It has a ReLu activation function and we include a 2D Max Pooling layer with a pool size of 2,2




print("Prior to flattening", model.output_shape)

#Layer 4 - A flatten layer.
#         This reshapes the array from a 4D to a 1D array
#         (We previously did this BEFORE during our data preperation, now we need to do it in our network itself)



print("After flattening", model.output_shape)

#Layer 5 - A Dense Layer with 64 Neurons, a ReLu activation function and a dropout of 0.5




#Layer 6 - An output layer with one output and a sigmoid activation layer
model.add(Dense(1))
model.add(Activation('sigmoid'))

# Picking a binary crossentropy loss function since we only have 2 classes
model.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])

# Print a summary of the network
print(model.summary())

# Generate a graph image of the network
plot_model(model, to_file='model.png')

# Display the image in the notebook
from IPython.display import Image
Image(filename='model.png')

# This is the data augmentation configuration we will use for training
train_datagen = ImageDataGenerator(
    rescale=1. / 255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True)

# This is the augmentation configuration we will use for validation:
# only perform a image rescaling
validation_datagen = ImageDataGenerator(rescale=1. / 255)


# Generates the data files for training with a binary class mode
train_generator =




# Generates the data files for validation with a binary class mode
validation_generator =




# Define the checkpoint
checkpoint = ModelCheckpoint('CNN_model.h5', monitor='val_acc',
                             verbose=1, save_best_only=True, save_weights_only=False, mode='max')
callbacks_list = [checkpoint]

# Train the model
# The double // operator is just an easy python way of dividing then rounding up/down to the nearest whole number
history = model.fit_generator(
    train_generator,
    steps_per_epoch=nb_train_samples // batch_size,
    epochs=epochs,
    validation_data=validation_generator,
    validation_steps=nb_validation_samples // batch_size,
    callbacks = callbacks_list)

# Plot training & validation accuracy values
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('Model accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()


# Plot training & validation loss values
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()


# Save the weights for future use
model.save_weights('CNN_hot_dog.h5')

# Now to run some predictions
# If you want to reload the weights, define the model again as above then load the weights
model.load_weights('CNN_hot_dog.h5')

 # Load a single image from a folder and convert to a numpy array

from PIL import Image
from tensorflow.keras.preprocessing import image
import numpy as np
predict_data_dir = 'data/test'
classes = list(train_generator.class_indices.keys())

predict_image = image.load_img(predict_data_dir + "/5.jpg")
predict_image_resized = predict_image.resize([img_width, img_height])

predict_image_array = image.img_to_array(predict_image_resized)

predict_image_array = np.expand_dims(predict_image_array, axis=0)

prediction = model.predict(predict_image_array, verbose=1)
prediction_index = prediction.argmax()

plt.imshow(predict_image)

print("The Predicted Value is", prediction)

print("The predicted item is a " + classes[int(prediction)])

# Lets predict the rest of the images
import os, os.path
predict_data_dir = 'data/test/'

list_of_images = os.listdir(predict_data_dir)


for i in range(len(list_of_images)):
    predict_image = image.load_img(predict_data_dir + list_of_images[i], target_size=(img_width, img_height))
    predict_image_resized = predict_image.resize([img_width, img_height])
    predict_image_array = image.img_to_array(predict_image_resized)
    predict_image_array = np.expand_dims(predict_image_array, axis=0)

    prediction = model.predict(predict_image_array, verbose=1)
    prediction_index = prediction.argmax()

    plt.figure()
    plt.imshow(predict_image)
    plt.show()
    print(prediction)

    print("The predicted item is a " + classes[int(prediction)] + "\n\n")
